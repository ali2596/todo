import React, { useState } from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import GoalItems from './components/GoalItems';
import GoalInput from './components/GoalInput';

export default function App() {
  const [courseGoals, setCourseGoals] = useState([]);

  const addGoalHandler = goalTitle => {
    if (goalTitle === ''){
      return 
    }
    else{
      setCourseGoals(currentGoals => [...currentGoals, { id: Math.random().toString(), value: goalTitle }]);
      console.log(goalTitle)
    }
    
  }

  const removeGoalHandler = goalId => {
    setCourseGoals(currentGoals => {
      return currentGoals.filter((goal) => goal.id !== goalId);
    })
  }

  return (
    <View style={styles.container}>
      <GoalInput onAddGoal={addGoalHandler} />

      <FlatList 
        keyExtractor={(item, index) => item.id}
        data={courseGoals}
        renderItem={itemData => <GoalItems id={itemData.item.id} onDelete={removeGoalHandler} text={itemData.item.value} />}
      />
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 50
    // flex: 1,
    // backgroundColor: '#fff',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
});
