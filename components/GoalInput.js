import React, { useState } from 'react';
import { TextInput, View, StyleSheet, Button, Image } from 'react-native';

const GoalInput = props => {
    const [goals, setGoals] = useState('');
    const goalInputHandler = (enterText) => {
        setGoals(enterText);
    }
    return (
        <View style={styles.container}>
            <Image style={styles.img} source={require('../assets/todo.jpg')}  />
            <View style={styles.inputContainer} >
                <TextInput
                    placeholder='Enter Goals'
                    style={styles.input}
                    onChangeText={goalInputHandler}
                    value={goals} >
                </TextInput>
                <Button title='ADD' onPress={props.onAddGoal.bind(this, goals)} />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    inputContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    input: {
        width: "80%",
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        padding: 10
    },
    img: {
        width: 150,
        height: 150,
    },
    container:{
        alignItems: "center",
    }
})

export default GoalInput